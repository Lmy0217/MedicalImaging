function predict(args, epoch, cindex)

argsindex = strfind(args,'-');

model = args(1:argsindex(1)-1);
if ~exist('epoch', 'var')
    epoch = 50;
end
cross = args(argsindex(1)+1:argsindex(2)-1);
crossfind = strfind(cross, '_');
cross_index = str2double(cross(1:crossfind(1)-1));
cross_count = str2double(cross(crossfind(1)+1:end));
clear argsindex cross crossfind

data_count = 10;
fold_length = floor(data_count / cross_count);
fold_start = (cross_index - 1) * fold_length + 1;
more_count = data_count - cross_count * fold_length;
if more_count ~= 0
    adding_step = more_count / cross_count;
    adding_count = floor((cross_index - 1) * adding_step);
    fold_start = fold_start + adding_count;
    fold_length = fold_length + floor(cross_index * adding_step) - adding_count;
    clear adding_step adding_count
end
clear more_count

datafolder = '../../datasets/MedicalImaging';
resultfolder = ['./save/', args];

if exist('cindex', 'var')
    fold_start = cindex;
    fold_length = 1;
end

if ~exist('data', 'var')
    data = {};
    for i = fold_start:fold_start+fold_length-1
        temp = load([datafolder, '/mat/sMRIs_', num2str(i), '.mat']);
        data = [data, temp.sMRIs];
    end
    clear temp
end
if ~exist('label', 'var')
    label = {};
    for i = fold_start:fold_start+fold_length-1
        temp = load([datafolder, '/mat/CtrlImgs_', num2str(i), '.mat']);
        label = [label, temp.CtrlImgs];
    end
    clear temp
end
if ~exist('predict', 'var')
    predict = load([resultfolder, '/', model, '_', num2str(epoch), '_predict.mat']);
    predict = permute(predict.predict, [1, 3, 4, 2]);
end
if ~exist('split', 'var')
    split = load([datafolder, '/split.mat']);
    split = split.split;
    splitcount = load([datafolder, '/splitcount.mat']);
    splitcount = splitcount.splitcount;
end

epochdir = [resultfolder, '/predict/', num2str(epoch)];
mkdir(epochdir);

colormap(jet)

for index = 1:size(data, 2)
    residual = abs(squeeze(predict(index,:,:,:)) - label{index}(:,:,:,1));
    %figure;
    viewslice(data{index}, [192,256,256], 1, 1, [0, 100],[],[30, 4],0);
    saveas(gcf, [epochdir, '/', num2str(split(splitcount(fold_start, 1) + index - 1), '%03d'), '_sMRI'], 'jpg');
    %figure;
    viewslice(label{index}(:,:,:,1), [64, 64, 21], 1, 1, [0, 2000],[],[6, 4],0);
    saveas(gcf, [epochdir, '/', num2str(split(splitcount(fold_start, 1) + index - 1), '%03d'), '_CtrlImg'], 'jpg');
    %figure;
    viewslice(squeeze(predict(index,:,:,:)), [64, 64, 21], 1, 1, [0, 2000],[],[6, 4],0);
    saveas(gcf, [epochdir, '/', num2str(split(splitcount(fold_start, 1) + index - 1), '%03d'), '_predict'], 'jpg');
    %figure;
    viewslice(residual, [64, 64, 21], 1, 1, [0, 2000],[],[6, 4],0);
    saveas(gcf, [epochdir, '/', num2str(split(splitcount(fold_start, 1) + index - 1), '%03d'), '_residual'], 'jpg');
end

% % figure;
% % [pdft, x] = ksdensity(residual(:));
% % plot(x, pdft);
% residual = residual(:);
% %residual = (residual - min(residual)) / (max(residual) - min(residual));
% s = std(residual);
% m = mean(abs(residual));
