# Medical Imaging
[![Travis](https://img.shields.io/travis/Lmy0217/MedicalImaging.svg?branch=master&label=Travis+CI)](https://www.travis-ci.org/Lmy0217/MedicalImaging) [![CircleCI](https://img.shields.io/circleci/project/github/Lmy0217/MedicalImaging.svg?branch=master&label=CircleCI)](https://circleci.com/gh/Lmy0217/MedicalImaging) [![License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](https://github.com/Lmy0217/MedicalImaging/pulls)

* This is a code backup repository until finish experiment and paper accepted.
* The medical images dataset has not open plan.
* The code is licensed with the [MIT](LICENSE) license.
