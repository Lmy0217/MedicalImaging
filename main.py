import argparse
import torch
import torch.optim as optim
import os
import platform
import Config
import models
import scipy.io
import numpy as np
import logging
from dataset import MI

parser = argparse.ArgumentParser(description='MI')
parser.add_argument('--model', type=str, default='cnn_7', metavar='M',
                    help='model name')
Config.default = Config.cfg(parser.parse_known_args()[0].model)
parser.add_argument('--ASL', type=str, default=Config.default.paths.ASL_file[0], metavar='ASL',
                    help='ASL (default: ' + str(Config.default.paths.ASL_file[0]) + ': '
                         + str(Config.default.paths.ASL_file) + ')')
parser.add_argument('--cross', type=int, default=Config.default.cross_index, metavar='X',
                    help='cross index')
parser.add_argument('--count', type=int, default=Config.default.cross_count, metavar='C',
                    help='cross count')
parser.add_argument('--batchsize', type=int, default=Config.default.batch_size, metavar='B',
                    help='batch size')
parser.add_argument('--testbatchsize', type=int, default=Config.default.test_batch_size, metavar='TB',
                    help='test batch size')
parser.add_argument('--epochs', type=int, default=Config.default.epochs, metavar='N',
                    help='number of epochs to train (default: ' + str(Config.default.epochs) + ')')
parser.add_argument('--lr', type=float, default=Config.default.lr, metavar='LR',
                    help='learning rate (default: ' + str(Config.default.lr) + ')')
parser.add_argument('--lrdecay', type=float, default=Config.default.lr_decay, metavar='LRD',
                    help='learning rate decay (default: ' + str(Config.default.lr_decay) + ')')
parser.add_argument('--momentum', type=float, default=Config.default.momentum, metavar='M',
                    help='SGD momentum (default: ' + str(Config.default.momentum) + ')')
parser.add_argument('--MTime', type=int, default=Config.default.sMRIs.time, metavar='MT',
                    help='MRI time')
parser.add_argument('--COverlapping', type=int, default=Config.default.ASL.overlapping, metavar='CO',
                    help='CtrlImg overlapping')
parser.add_argument('--slide', action='store_false' if Config.default.sMRIs.slide else 'store_true',
                    default=Config.default.sMRIs.slide, help='slide')
parser.add_argument('--kT', type=int, default=Config.default.kernel.kT, metavar='kT',
                    help='kT')
parser.add_argument('--kW', type=int, default=Config.default.kernel.kW, metavar='kW',
                    help='kW')
parser.add_argument('--kH', type=int, default=Config.default.kernel.kH, metavar='kH',
                    help='kH')
parser.add_argument('--dT', type=int, default=Config.default.kernel.dT, metavar='dT',
                    help='dT')
parser.add_argument('--dW', type=int, default=Config.default.kernel.dW, metavar='dW',
                    help='dW')
parser.add_argument('--dH', type=int, default=Config.default.kernel.dH, metavar='dH',
                    help='dH')
parser.add_argument('--reset', action='store_false' if Config.default.data_reset else 'store_true',
                    default=Config.default.data_reset, help='reset')
parser.add_argument('--no-cuda', action='store_false' if Config.default.nocuda else 'store_true',
                    default=Config.default.nocuda, help='disables CUDA training')
parser.add_argument('--seed', type=int, default=Config.default.seed, metavar='S',
                    help='random seed (default: ' + str(Config.default.seed) + ')')
parser.add_argument('--ci', action='store_false' if Config.default.ci else 'store_true', default=Config.default.ci,
                    help='running CI')
args = parser.parse_args()
cfg = Config.args2dataset(args)
if cfg.ci:
    torch.set_default_tensor_type(torch.DoubleTensor)
    cfg.batch_size = 8
    cfg.test_batch_size = 8
    cfg.epochs = 1

cfg.cuda = not cfg.nocuda and torch.cuda.is_available()

torch.manual_seed(cfg.seed)
device = torch.device("cuda" if cfg.cuda else "cpu")

save_folder = os.path.join(cfg.root_folder, cfg.paths.save_folder, args.model + '-' + str(cfg.paths.ASL_file[0]) + '-' \
                           + str(cfg.cross_index) + '_' + str(cfg.cross_count) + '-' + str(cfg.batch_size) + '-' \
                           + str(cfg.sMRIs.time) + '-' + str(cfg.ASL.overlapping) + '-' + str(cfg.kernel.kT) + '_' \
                           + str(cfg.kernel.kW) + '_' + str(cfg.kernel.kH) + '_' + str(cfg.kernel.dT) + '_' \
                           + str(cfg.kernel.dW) + '_' + str(cfg.kernel.dH) + '-' + str(cfg.lr) + '-' \
                           + str(cfg.momentum) + '-' + str(cfg.seed))
if not os.path.exists(save_folder):
    os.makedirs(save_folder)

logging_file = os.path.join(save_folder, args.model + cfg.paths.logging_file)
if not os.path.exists(logging_file):
    if platform.system() == 'Windows':
        open(logging_file, 'w')
    else:
        os.mknod(logging_file)

logging.basicConfig(level=logging.INFO, format='%(asctime)s : %(message)s', datefmt='%Y-%m-%d %A %H:%M:%S',
                    filename=logging_file, filemode='a')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s :  %(message)s')
console.setFormatter(formatter)
logging.getLogger().addHandler(console)

logging.info(cfg)

kwargs = {'num_workers': 1, 'pin_memory': True} if cfg.cuda else {}
trainset = MI(data_type='train', cfg=cfg)
testset = MI(data_type='test', cfg=trainset.cfg, ms=trainset.ms, transform=trainset.transform,
             target_transform=trainset.target_transform)
train_loader = torch.utils.data.DataLoader(trainset, batch_size=cfg.batch_size, shuffle=True, **kwargs)
test_loader = torch.utils.data.DataLoader(testset, batch_size=cfg.test_batch_size, shuffle=True, **kwargs)

model = models.create_model(args.model, cfg=cfg)

if os.path.exists(os.path.join(save_folder, args.model + cfg.paths.check_file)):
    start_epoch = torch.load(os.path.join(save_folder, args.model + cfg.paths.check_file))
    model.load_state_dict(torch.load(os.path.join(save_folder, args.model + '_' + str(start_epoch) + '.pth')))
else:
    start_epoch = 0

model = model.to(device)

optimizer = optim.Adam(model.parameters(), lr=cfg.lr)


def train(epoch):
    model.train()
    criterion = torch.nn.SmoothL1Loss(size_average=False)
    log_step = 100 if not cfg.ci else 1
    for batch_idx, (data, target, _) in enumerate(train_loader):
        data, target, criterion = data.to(device), target.to(device), criterion.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = criterion(output.view(target.shape), target)
        loss.backward()
        optimizer.step()
        if batch_idx % log_step == 0:
            logging.info('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                        epoch, (batch_idx + 1) * len(data), len(train_loader.dataset),
                        100. * (batch_idx + 1) / len(train_loader), loss.item() / output.view(-1).shape[0]))

    if not os.path.exists(save_folder):
        os.makedirs(save_folder)
    torch.save(model.state_dict(), os.path.join(save_folder, args.model + '_' + str(epoch) + '.pth'))
    torch.save(epoch, os.path.join(save_folder, args.model + cfg.paths.check_file))


def test(epoch):
    model.eval()
    criterion = torch.nn.SmoothL1Loss(size_average=False)
    test_loss = 0
    predict = torch.Tensor(testset.count, cfg.ASL.time, cfg.ASL.width, cfg.ASL.height)
    log_step = 10 if not cfg.ci else 1
    with torch.no_grad():
        for batch_idx, (data, target, index) in enumerate(test_loader):
            data, target, criterion, predict = data.to(device), target.to(device), criterion.to(device), predict.to(device)
            output = model(data)
            output = output.view(target.shape)
            test_loss += criterion(output, target).item()
            if batch_idx % log_step == 0:
                logging.info('Test Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                              epoch, (batch_idx + 1) * len(data), len(test_loader.dataset),
                              100. * (batch_idx + 1) / len(test_loader), test_loss / ((batch_idx + 1) * output.view(-1).shape[0])))
            for i in range(len(output)):
                predict[index[i, 0], index[i, 1]:index[i, 2], index[i, 3]:index[i, 4], index[i, 5]:index[i, 6]] = output[i]

    if not cfg.nonorm and testset.ms is not None:
        for i in range(0, predict.shape[1]):
            predict[:, i, :, :] = predict[:, i, :, :] * testset.ms[1][1, i] + testset.ms[1][0, i]

    test_loss /= len(test_loader.dataset) * testset.okT * testset.okW * testset.okH
    logging.info('Test set: Average loss: {:.4f}'.format(test_loss))
    predict_file = os.path.join(save_folder, args.model + '_' + str(epoch) + cfg.paths.predict_file)
    scipy.io.savemat(predict_file, {'predict': np.array(predict)})


if __name__ == '__main__':
    for epoch in range(start_epoch + 1, cfg.epochs + 1):
        train(epoch)
        #test(epoch)

    test(cfg.epochs)
    #test(50)
