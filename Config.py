

__ASL_FILE_MAP__ = {
    'C': 'CtrlImgs_',
    'L': 'LabelImgs_',
}


class paths(object):

    def __init__(self, data_folder='datasets', mat_folder='mat', sMRIs_file='sMRIs_', ASL_file=__ASL_FILE_MAP__['C'],
                 file_type='_c.mat', save_folder='save', check_file='_checkpoint.pth', ms_file='_ms.pth',
                 predict_file='_predict.mat', logging_file='_logging.log'):
        self.data_folder = data_folder
        self.mat_folder = mat_folder
        self.sMRIs_file = sMRIs_file
        self.ASL_file = ASL_file
        self.file_type = file_type
        self.save_folder = save_folder
        self.check_file = check_file
        self.ms_file = ms_file
        self.predict_file = predict_file
        self.logging_file = logging_file

    def __eq__(self, other):
        if isinstance(other, paths):
            return self.data_folder == other.data_folder and self.mat_folder == other.mat_folder \
                   and self.sMRIs_file == other.sMRIs_file and self.ASL_file == other.ASL_file \
                   and self.file_type == other.file_type and self.save_folder == other.save_folder \
                   and self.check_file == other.check_file and self.ms_file == other.ms_file \
                   and self.predict_file == other.predict_file and self.logging_file == other.logging_file
        else:
            return False

    def __repr__(self):
        return '  paths: {\n' \
               + '    data_folder: ' + self.data_folder + '\n' \
               + '    mat_folder: ' + self.mat_folder + '\n' \
               + '    sMRIs_file: ' + self.sMRIs_file + '\n' \
               + '    ASL_file: ' + self.ASL_file + '\n' \
               + '    file_type: ' + self.file_type + '\n' \
               + '    save_folder: ' + self.save_folder + '\n' \
               + '    check_file: ' + self.check_file + '\n' \
               + '    ms_file: ' + self.ms_file + '\n' \
               + '    predict_file: ' + self.predict_file + '\n' \
               + '    logging_file: ' + self.logging_file + '\n' \
               + '  }\n'


class kernel(object):

    def __init__(self, kT, kW, kH, dT, dW, dH):
        self.kT = kT
        self.kW = kW
        self.kH = kH
        self.dT = dT
        self.dW = dW
        self.dH = dH

    def __eq__(self, other):
        if isinstance(other, kernel):
            return self.kT == other.kT and self.kW == other.kW and self.kH == other.kH \
                   and self.dT == other.dT and self.dW == other.dW and self.dH == other.dH
        else:
            return False

    def __repr__(self):
        return '  kernel: {\n' \
               + '    kT: ' + str(self.kT) + '\n' \
               + '    kW: ' + str(self.kW) + '\n' \
               + '    kH: ' + str(self.kH) + '\n' \
               + '    dT: ' + str(self.dT) + '\n' \
               + '    dW: ' + str(self.dW) + '\n' \
               + '    dH: ' + str(self.dH) + '\n' \
               + '  }\n'


class sMRIs(object):

    def __init__(self, width, height, time, tBlock):
        self.width = width
        self.height = height
        self.time = time
        self.tBlock = tBlock
        self.slide = self.tBlock is not None

    def __eq__(self, other):
        if isinstance(other, sMRIs):
            return self.width == other.width and self.height == other.height and self.time == other.time \
                   and self.tBlock == other.tBlock and self.slide == other.slide
        else:
            return False

    def __repr__(self):
        return '  sMRIs: {\n' \
               + '    width: ' + str(self.width) + '\n' \
               + '    height: ' + str(self.height) + '\n' \
               + '    time: ' + str(self.time) + '\n' \
               + '    tBlock: ' + str(self.tBlock) + '\n' \
               + '    slide: ' + str(self.slide) + '\n' \
               + '  }\n'


class ASL(object):

    def __init__(self, width, height, time, overlapping):
        self.width = width
        self.height = height
        self.time = time
        self.overlapping = overlapping

    def __eq__(self, other):
        if isinstance(other, ASL):
            return self.width == other.width and self.height == other.height and self.time == other.time \
                   and self.overlapping == other.overlapping
        else:
            return False

    def __repr__(self):
        return '  ASL: {\n' \
               + '    width: ' + str(self.width) + '\n' \
               + '    height: ' + str(self.height) + '\n' \
               + '    time: ' + str(self.time) + '\n' \
               + '    overlapping: ' + str(self.overlapping) + '\n' \
               + '  }\n'


class Dataset(object):

    def __init__(self, kernel, sMRIs, ASL, batch_size, test_batch_size, epochs, lr, lr_decay, momentum, is3d=False,
                 nonorm=False, nocuda=False, seed=1, ci=False, paths=paths(), root_folder='.', data_count=10,
                 cross_index=1, cross_count=5, data_reset=False):
        self.kernel = kernel
        self.sMRIs = sMRIs
        self.ASL = ASL
        self.batch_size = batch_size
        self.test_batch_size = test_batch_size
        self.epochs = epochs
        self.lr = lr
        self.lr_decay = lr_decay
        self.momentum = momentum
        self.is3d = is3d
        self.nonorm = nonorm
        self.nocuda = nocuda
        self.seed = seed
        self.ci = ci
        self.paths = paths
        self.root_folder = root_folder
        self.data_count = data_count
        self.cross_index = cross_index
        self.cross_count = cross_count
        self.data_reset = data_reset

    def __eq__(self, other):
        if isinstance(other, Dataset):
            return self.kernel == other.kernel and self.sMRIs == other.sMRIs \
                   and self.ASL == other.ASL and self.batch_size == other.batch_size \
                   and self.test_batch_size == other.test_batch_size and self.lr == other.lr \
                   and self.lr_decay == other.lr_decay and self.epochs == other.epochs \
                   and self.momentum == other.momentum and self.is3d == other.is3d \
                   and self.nonorm == other.nonorm and self.nocuda == other.nocuda \
                   and self.seed == other.seed and self.ci == other.ci \
                   and self.paths == other.paths and self.root_folder == other.root_folder \
                   and self.data_count == other.data_count and self.cross_index == other.cross_index \
                   and self.cross_count == other.cross_count and self.data_reset == other.data_reset
        else:
            return False

    def __repr__(self):
        return 'Dataset: {\n' \
               + str(self.kernel) \
               + str(self.sMRIs) \
               + str(self.ASL) \
               + '  batch_size: ' + str(self.batch_size) + '\n' \
               + '  test_batch_size: ' + str(self.test_batch_size) + '\n' \
               + '  epochs: ' + str(self.epochs) + '\n' \
               + '  lr: ' + str(self.lr) + '\n' \
               + '  lr_decay: ' + str(self.lr_decay) + '\n' \
               + '  momentum: ' + str(self.momentum) + '\n' \
               + '  is3d: ' + str(self.is3d) + '\n' \
               + '  nonorm: ' + str(self.nonorm) + '\n' \
               + '  nocuda: ' + str(self.nocuda) + '\n' \
               + '  seed: ' + str(self.seed) + '\n' \
               + '  ci: ' + str(self.ci) + '\n' \
               + str(self.paths) \
               + '  root_folder: ' + str(self.root_folder) + '\n' \
               + '  data_count: ' + str(self.data_count) + '\n' \
               + '  cross_index: ' + str(self.cross_index) + '\n' \
               + '  cross_count: ' + str(self.cross_count) + '\n' \
               + '  data_reset: ' + str(self.data_reset) + '\n' \
               + '}\n'


def resnet_18():
    k = kernel(64, 64, 64, 8, 9, 13)
    M = sMRIs(192, 256, 256 - 24, [ 1, 12, 23, 33, 43, 52, 60, 67, 73, 79, 85, 91, 97, 103, 110, 118, 127, 137, 147, 158, 169 ])
    C = ASL(64, 64, 21, 2)
    dataset = Dataset(k, M, C, 96, 96, 30, 0.01, 0, 0, nonorm=True)
    return dataset


def resnet_34():
    k = kernel(64, 64, 64, 8, 9, 13)
    M = sMRIs(192, 256, 256 - 24, [ 1, 12, 23, 33, 43, 52, 60, 67, 73, 79, 85, 91, 97, 103, 110, 118, 127, 137, 147, 158, 169 ])
    C = ASL(64, 64, 21, 2)
    dataset = Dataset(k, M, C, 96, 96, 30, 0.01, 0, 0, nonorm=True)
    return dataset


def resnet_50():
    k = kernel(64, 64, 64, 8, 9, 13)
    M = sMRIs(192, 256, 256 - 24, [ 1, 12, 23, 33, 43, 52, 60, 67, 73, 79, 85, 91, 97, 103, 110, 118, 127, 137, 147, 158, 169 ])
    C = ASL(64, 64, 21, 2)
    dataset = Dataset(k, M, C, 32, 32, 30, 0.01, 0, 0, nonorm=True)
    return dataset


def resnet_101():
    k = kernel(64, 64, 64, 8, 9, 13)
    M = sMRIs(192, 256, 256 - 24, [ 1, 12, 23, 33, 43, 52, 60, 67, 73, 79, 85, 91, 97, 103, 110, 118, 127, 137, 147, 158, 169 ])
    C = ASL(64, 64, 21, 2)
    dataset = Dataset(k, M, C, 16, 16, 30, 0.01, 0, 0, nonorm=True)
    return dataset


def resnet_152():
    k = kernel(64, 64, 64, 8, 9, 13)
    M = sMRIs(192, 256, 256 - 24, [ 1, 12, 23, 33, 43, 52, 60, 67, 73, 79, 85, 91, 97, 103, 110, 118, 127, 137, 147, 158, 169 ])
    C = ASL(64, 64, 21, 2)
    dataset = Dataset(k, M, C, 16, 16, 30, 0.01, 0, 0, nonorm=True)
    return dataset


def cnn_7():
    k = kernel(12, 8, 8, 12, 6, 8)
    M = sMRIs(192, 256, 256 - 4, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 5120, 51200, 50, 0.01, 0, 0)
    return dataset


def cnn_12():
    k = kernel(12, 8, 8, 12, 6, 8)
    M = sMRIs(192, 256, 256 - 4, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 5120, 51200, 50, 0.01, 0, 0)
    return dataset


def capsule():
    k = kernel(12, 28, 28, 12, 23, 15)
    M = sMRIs(192, 256, 256 - 4, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 128, 128, 10, 0.01, 0, 0)
    return dataset


def capsule_w():
    k = kernel(12, 28, 28, 12, 23, 15)
    M = sMRIs(192, 256, 256 - 4, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 32, 32, 10, 0.01, 0, 0)
    return dataset


def capsule_7():
    k = kernel(12, 28, 28, 12, 23, 15)
    M = sMRIs(192, 256, 256 - 4, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 256, 256, 10, 0.01, 0, 0)
    return dataset


def capsule_12():
    k = kernel(12, 28, 28, 12, 23, 15)
    M = sMRIs(192, 256, 256 - 4, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 128, 128, 10, 0.01, 0, 0)
    return dataset


def capsule_3():
    k = kernel(12, 28, 28, 12, 23, 15)
    M = sMRIs(192, 256, 256 - 4, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 128, 128, 50, 0.01, 0, 0)
    return dataset


def capsule_3w():
    k = kernel(12, 28, 28, 12, 23, 15)
    M = sMRIs(192, 256, 256 - 4, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 192, 192, 6, 0.01, 0, 0)
    return dataset


def capsule_4():
    k = kernel(12, 28, 28, 12, 23, 15)
    M = sMRIs(192, 256, 256 - 4, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 128, 128, 50, 0.01, 0, 0)
    return dataset


def capsule_5():
    k = kernel(12, 28, 28, 12, 23, 15)
    M = sMRIs(192, 256, 256 - 4, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 512, 512, 10, 0.01, 0, 0)
    return dataset


def squeezenet_v10():
    k = kernel(64, 64, 64, 8, 9, 13)
    M = sMRIs(192, 256, 256 - 24, [ 1, 12, 23, 33, 43, 52, 60, 67, 73, 79, 85, 91, 97, 103, 110, 118, 127, 137, 147, 158, 169 ])
    C = ASL(64, 64, 21, 2)
    dataset = Dataset(k, M, C, 96, 96, 30, 0.01, 0, 0, nonorm=True)
    return dataset


def squeezenet_v11():
    k = kernel(64, 64, 64, 8, 9, 13)
    M = sMRIs(192, 256, 256 - 24, [ 1, 12, 23, 33, 43, 52, 60, 67, 73, 79, 85, 91, 97, 103, 110, 118, 127, 137, 147, 158, 169 ])
    C = ASL(64, 64, 21, 2)
    dataset = Dataset(k, M, C, 96, 96, 30, 0.01, 0, 0, nonorm=True)
    return dataset


def fullscale():
    k = kernel(64, 192, 256, 0, 192, 256)
    M = sMRIs(192, 256, 256 - 24, [ 1, 12, 23, 33, 43, 52, 60, 67, 73, 79, 85, 91, 97, 103, 110, 118, 127, 137, 147, 158, 169 ])
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 2, 2, 30, 0.01, 0, 0, nonorm=True)
    return dataset


def cnn3d_3():
    k = kernel(232, 192, 256, 232, 192, 256)
    M = sMRIs(192, 256, 256 - 24, None)
    C = ASL(64, 64, 21, 1)
    dataset = Dataset(k, M, C, 1, 1, 50, 0.01, 0, 0, is3d=True, nonorm=True)
    return dataset


default = cnn_7()


def cfg(model=None):
    if model is not None:
        return eval("" + model + "()")
    else:
        return default


def args2dataset(args):
    k = kernel(kT=args.kT, kW=args.kW, kH=args.kH, dT=args.dT, dW=args.dW, dH=args.dH)
    M = sMRIs(width=default.sMRIs.width, height=default.sMRIs.height, time=args.MTime, tBlock=default.sMRIs.tBlock if args.slide else None)
    C = ASL(width=default.ASL.width, height=default.ASL.height, time=default.ASL.time, overlapping=args.COverlapping)
    config_paths = paths(ASL_file=__ASL_FILE_MAP__[args.ASL])
    dataset = Dataset(k, M, C, cross_index=args.cross, cross_count=args.count, data_count=default.data_count,
                      paths=config_paths, root_folder=default.root_folder, batch_size=args.batchsize,
                      test_batch_size=args.testbatchsize, epochs=args.epochs, lr=args.lr, lr_decay=args.lrdecay,
                      momentum=args.momentum, is3d=default.is3d, nonorm=default.nonorm, nocuda=args.no_cuda,
                      seed=args.seed, ci=args.ci, data_reset=args.reset)
    return dataset


if __name__ == "__main__":
    print(cfg())
